<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'admin'] ], function () {
    Route::get('/admin/home', 'HomeController@admin')->name('cpanel');
    Route::resource('room', 'RoomController');
});
Route::group(['prefix' => 'member'], function () {
    Route::get('/rooms', 'RoomReservationController@show');
    Route::patch('/room/extension', 'RoomReservationController@updateMemberRoom');
    Route::post('/room/complate/{id}', 'RoomReservationController@complateMemberRoom');
    Route::get('/show/partners/{id}', 'RoomReservationController@showPartners');
});
Route::group(['prefix' => 'rooms'], function () {
    Route::post('/reservation', 'RoomReservationController@reservation')->name('room.reservation');
    Route::get('/show_time_table/{id}', 'RoomReservationController@showTimeTable')->name('room.showtimetable');
    Route::get('/show/member_rooms/{id}', 'RoomReservationController@showMemberRooms')->name('member.rooms');
    Route::get('/status', 'RoomReservationController@getRoomStatus');
});