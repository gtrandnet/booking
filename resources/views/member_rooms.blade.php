@extends('layouts.app')

@section('content')
<div class="container">
<member-rooms :id="{{Auth::user()->id}}"></member-rooms>
</div>
@endsection