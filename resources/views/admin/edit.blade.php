@extends('layouts.app')

@section('content')
@if(isset($room))
<div class="container">
    <update-room :title="{{json_encode($room->title)}}" :id="{{json_encode($room->id)}}"></update-room>
</div>
@else
<div class="container">
    <update-room></update-room>
</div>
@endif
@endsection
