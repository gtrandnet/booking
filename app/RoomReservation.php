<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomReservation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'room_id', 'user_name', 'room_title', 'status', 'booking_start', 'booking_finish'];

    public function rooms(){
    	return $this->hasMany(Room::class);
    }
    public function partners(){
    	return $this->hasMany(Partner::class);
    }
}
