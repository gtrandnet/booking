<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Helper;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $row = Room::all();
        Helper::changeStatus($row);
        
        return response(Room::all()->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|unique:rooms|max:255'
        ]);

        $room = new Room;
        $room->title = $request->title;
        $room->status = 0;
        $room->save();
        return "Комната успешно создана!";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        return view('admin.edit', compact('room'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $room = Room::findOrFail($request->id);
        $room->title = $request->title;
        $room->status = 0;
        $room->save();
        return "Комната успешно изменена!";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Room::destroy($id); 

        return response('Комната удалена!', Response::HTTP_OK);
    }
}
