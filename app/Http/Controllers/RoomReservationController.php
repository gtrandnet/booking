<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\RoomReservation;
use App\Services\Helper;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;

class RoomReservationController extends Controller
{
    public function reservation(Request $request)
    {
        $start  = $request->start;
        $finish = $request->finish;
        
        if(strtotime($start) >= strtotime($finish) or strtotime($start) <= Carbon::now()->timestamp)
            return 'Неправильный формат даты, пожалуйста выберите правильную дату!!';

        $response = Helper::isValid($start, $finish, $request->room_id);
        if(!$response)
            return "Комната занята!";
        

        $reservation = new RoomReservation;
        $reservation->user_id = Auth::user()->id;
        $reservation->user_name = Auth::user()->name;
        $reservation->room_id = $request->room_id;
        $reservation->room_title = $request->room_title;
        $reservation->status = 1;
        $reservation->booking_start = $start;
        $reservation->booking_finish = $finish;
        $reservation->save();

        $room_reservation_id = RoomReservation::latest('id')->first('id');
        $partner = Helper::setPartners($request->partners, $room_reservation_id->id);
        //return response(json_encode($request->partners), Response::HTTP_OK);
        return "Вы успешно забронировали комнату!";
    }

    public function showTimeTable($id)
    {
        $data = RoomReservation::where('room_id', $id)
                                                    ->where('status', 1)->get();

        return response($data->jsonSerialize(), Response::HTTP_OK);
    }

    public function showMemberRooms($id)
    {
        $data = RoomReservation::where('user_id', $id)
                                                    ->where('status', 1)->get();

        return response($data->jsonSerialize(), Response::HTTP_OK);
    }


    public function show()
    {
        return view('member_rooms');
    }

    public function updateMemberRoom(Request $request)
    {
        $validFinishTime = RoomReservation::whereId($request->id)
                                            ->get('booking_finish')
                                            ->toArray();
        $start = $request->start;
        $finish = $request->finish;
        $v = $validFinishTime[0]['booking_finish'];
        $id = $request->id;
        if(strtotime($start) > strtotime($finish) or strtotime($finish) < strtotime($v))
            return 'Неправильный формат даты, пожалуйста выберите правильную дату!!';

        $response = Helper::isValid($start, $finish, $request->room_id, $id);
        if(!$response)
            return 'В это время комната занято, пожалуйста выберите другую дату!!';

        $row = [
            'user_name' => Auth::user()->name,
            'room_title' => $request->room_title,
            'status' => 1,
            'booking_start' => $start,
            'booking_finish' => $finish
        ];
        RoomReservation::whereId($id)->update($row);

        return 'Комната успешно продлена';
    }

    public function complateMemberRoom($id)
    {
        $data = RoomReservation::findOrFail($id);
        $data->status = 0;
        $data->save();

        return "Успешное завершение!";
    }

    public function getRoomStatus()
    {
        //dd($x = Carbon::parse('2019-10-17 10:56:00')->timestamp);
        $res = [];
        $data = RoomReservation::where('status', 1)->get();
        foreach($data as $row )
        {
            if(Helper::isToday($row->booking_start, $row->booking_finish))
            {
                $res[$row->room_title]['start'] = date('h:i',strtotime($row->booking_start));
                $res[$row->room_title]['finish'] = date('h:i',strtotime($row->booking_finish));
                $res[$row->room_title]['id'] = $row->room_id;
            }
        }
        
        return json_encode($res);
    }
    
    public function showPartners($id)
    {
        $data = RoomReservation::findOrfail($id)->partners;
        $row = [];
        if(!$data)
            return json_encode($row);
        return response($data->jsonSerialize(), Response::HTTP_OK);

    }





}
