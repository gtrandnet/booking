<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'image', 'status'];

    public function roomreservation(){
    	return $this->hasMany(RoomReservation::class);
    }
}
